import { ButtonsBox, PageTemplate, Pause, Play, PlayerTemplate } from './atoms';

export { ButtonsBox, PageTemplate, Pause, Play, PlayerTemplate };