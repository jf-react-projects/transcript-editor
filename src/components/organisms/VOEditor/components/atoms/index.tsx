import ButtonsBox from './ButtonsBox/ButtonsBox';
import PageTemplate from './PageTemplate/PageTemplate';
import Pause from './Pause/Pause';
import Play from './Play/Play';
import PlayerTemplate from './PlayerTemplate/PlayerTemplate';

export { ButtonsBox, PageTemplate, Pause, Play, PlayerTemplate };